## Overview

The following will go over the process of upgrading/changing firmware for the DT7 between drone mode and RoboMaster mode. The DT7 has two main types of firmware as of 4/4/22:
- v1.0.2.26 for DJI drone flight controllers
- v3.0.3.7 for RoboMaster client

## Required Software

The only tool needed is the DJI RC Assistant found here: https://www.dji.com/downloads/softwares/assistant-dt7-dr16-rc-system

## Steps to install RoboMaster firmware:
0. Plug in the DT7 remote to the computer using a USB mini-b cable
1. Open RC Assistant and navigate to the "Info" page: 

<img src="uploads/RCTool_1.png" alt="RCTool 1" height="200">  

2. On "Change User", login to your DJI Account (the one you use to register)
3. Click the "Latest Version: &lt;XXX&gt;"
4. On the firware upgrade page, wait for the version to load, then type the buttons <kbd>D</kbd> <kbd>O</kbd> <kbd>O</kbd> <kbd>R</kbd> on the keyboard to enter the backdoor.

<img src="uploads/RCTool_2.png" alt="RCTool 2" height="200">  

5. When the login screen pops up, enter 'rm' as the username and password.

<img src="uploads/RCTool_3.png" alt="RCTool 3" height="200"> 

6. Click "Upgrade" and follow the prompts to install the version 3.0.3.7 firmware.

## Steps to install aircraft firmware:
0. Plug in the DT7 remote to the computer using a USB mini-b cable
1. Open RC Assistant and navigate to the "Info" page: 
2. On "Change User", login to your DJI Account (the one you use to register)
3. Click the "Latest Version: &lt;XXX&gt;"
6. Click "Upgrade" and follow the prompts to install the version 1.0.2.26 firmware.
