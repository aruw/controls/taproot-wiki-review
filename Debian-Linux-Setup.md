We strongly recommend Ubuntu 20.04 LTS. This guide should also work with other recent Debian-based
distributions. Older Ubuntu versions are likely to _not_ work due to missing modern gcc/g++.

1. Run the following commands in a terminal:
    ```sh
    sudo apt-get install python3 python3-pip scons git openocd gcc build-essential libboost-all-dev
    sudo apt-get --no-install-recommends install doxygen
    ```

1. Download the latest version of the `arm-none-eabi` toolchain from [here](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).

  You want the file ending in `-x86_64-linux.tar.bz2`. Extract the file with the Archive Manager GUI
  or via the command line, as `tar -xjf /*-x86_64-linux.tar.bz2`. Put the extracted directory
  somewhere you can refer back to later.

1. Add the new compiler and tools to your PATH so they're accessible in every terminal.

   Run the following command, replacing `path/to/gcc-arm-none-eabi` with your absolute path to the
   toolchain, e.g. `/home/foo/gcc-arm-none-eabi-10-2020-q4-major`. Don't forget the `/bin` on the
   end!

   ```sh
   echo 'export PATH="path/to/gcc-arm-none-eabi/bin:$HOME/.local/bin:$PATH:$PATH"' >> ~/.bashrc
   source ~/.bashrc
   ```

1. Choose or create a directory to store your source code. `repos` is a good name choice:

   ```sh
   mkdir repos
   cd repos
   ```

1. Build and install googletest:

   ```sh
   git clone --branch release-1.10.0 https://github.com/google/googletest.git
   cd googletest
   mkdir build
   cd build
   cmake ../
   make
   sudo make install
   cd ../../
   ```

1. Download and compile the latest version of openocd (`apt-get install openocd` is insufficient).

   ```sh
   git clone https://github.com/ntfreak/openocd.git
   cd openocd
   ./bootstrap
   ./configure
   make
   sudo make  install
   cd ../
   ```

1. Install the ST-Link V2 driver. This is done through the terminal. The following instructions
   were taken and partially modified from [this
   site](https://freeelectron.ro/installing-st-link-v2-to-flash-stm32-targets-on-linux/).

   ```sh
   sudo apt-get install git make cmake libusb-1.0-0-dev
   sudo apt-get install gcc build-essential
   git clone https://github.com/texane/stlink
   cd stlink
   cmake .
   make
   sudo make install
   cd bin
   sudo cp st-* /usr/local/bin
   cd ..
   sudo cp config/udev/rules.d/49-stlinkv* /etc/udev/rules.d/ 
   sudo udevadm control --reload
   cd ..
   ```

1. Download and install [VSCode](https://code.visualstudio.com/download).

Visit the README of the repo you are working in to find next steps. You'll want to:
- Clone your repository
- Build the project and run the tests to confirm your environment is set up correctly
