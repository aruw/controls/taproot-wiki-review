The Taproot library is constantly evolving, and we recommend occasionally upgrading to more recent
versions to keep up with new features and improvements. This is good to do during development but
likely best avoided immediately preceding a competition!

Upgrading your project is a manual process. There are three steps:
- Update your project settings, tool configurations, and build scripts
- Bump the version of Taproot that your project includes and re-generate it
- Try to build your project and run your tests; fix what is broken

These should be done from a clean workspace and in individual commits.

## Before you begin

**Note: this process _requires_ a Linux or macOS host. Running lbuild on Windows will produce
incorrect path separators (backslashes) for non-Windows hosts. See [here](https://gitlab.com/aruw/controls/taproot/-/issues/110)
for tracking a fix. In the mean time, the Docker container or WSL will work fine.**

Ensure you have a **clean** working tree. This means:

- All changes committed
- All commits pushed

This is important, since we don't want to lose work.

We also strongly recommend starting a new branch now and introducing your changes in a Merge
Request. If you are using our GitLab Pipeline, this will also validate that you didn't break your
project.

## Upgrading project infrastructure

Inspect the history of merged MRs from our template project, listed here: https://gitlab.com/aruw/controls/taproot-template-project/-/merge_requests?scope=all&state=merged

For each one merged since the last time you upgraded, manually apply the changes made to all files,
_except for_ those in the `taproot` directory. You can often click the "commits" tab and reference
all but one of the commits -- the one marked "lbuild build" or "regenerate taproot" is the only one
which makes changes you _don't_ need.

Again, note that the `taproot` directory within your project doesn't need to be touched -- those files
will be automatically generated in the next step. Also note that you can ignore the update of the
submodule itself (it will show as a one-line file with a long hexadecimal string changing).

In the future, we hope to improve this process. However, for now, it will have to be done manually.
If you have feedback on this, please provide it here: https://gitlab.com/aruw/controls/taproot/-/issues/109.

**Before proceeding, add and commit all changes.** It is likely the project doesn't build; this is
OK.

## Upgrading Taproot itself

Recall that Taproot projects have two "copies" of the library: one _git submodule_ at the top level,
and one generated instance of it within a folder like `my-project`. Below, `my-project-repo` is the
top-level repository folder, and `my-project` is the subfolder.

**Reminder: make sure that all work not related to this upgrade has been committed _and_ pushed.**

Run the following:

```sh
# Bump the submodule referenced version
cd my-project-repo/taproot
git checkout develop
git pull
git submodule update --init --recursive
cd ..

# Mark the version change to be committed
git add taproot

# IMPORTANT: switch into your project sub-folder
cd my-project

# Delete the entire taproot folder and re-generate it
# This is the Taproot folder within your project subfolder, _not_ at the root of the repo.
rm -r ./taproot
pipenv run lbuild build

# Commit the result
git add taproot
git commit -m "Regenerate Taproot"
```

## Fixing the breakage

Your goal is to get the following builds to succeed and tests to pass, when executed from your
project directory:
- `pipenv run scons build`
- `pipenv run scons run-tests`

Many/most errors should be fairly easy to work through. The [Taproot CHANGELOG](https://gitlab.com/aruw/controls/taproot/-/blob/develop/CHANGELOG.md)
is a good reference to see what might have caused code breakage. Please reach out to us on Discord
with any questions!
