## Overview

**When done improperly, debugging code that is not fully tested can be very dangerous.** The robots
we work with are large, weighing thirty to seventy pounds and have moving parts that can seriously
damage people if code is ran on a robot without proper safety procedures. Safe testing procedures
are  described below.

## The one rule of a very large thumb

When testing code, you should **always assume whatever code you have written is broken** and will
completely fail in the most epic way possible. This allows you to think about what could happen if
something goes wrong and plan accordingly.

### A short exposition

Suppose you are testing some new turret algorithm, the worst that I could see happening is the
turret spins out of control immediately and all the other motors and actuators connected to the
robot start going crazy. If you did not take any precaution while testing for the first time and
threw your code on a robot, the robot is now driving around out of control, wires are ruined from
the spinning turret, the turret's expensive yaw motor is burnt out and will need to be replaced, and
additional irreprehensible damage has been done to the other systems that went haywire. How can this
be avoided? That is what this document has been designed to teach you.

## A list of safety practices that should always be followed

- No matter what you are doing, **always prop the robot up on blocks** until code has been
  completely verified. Runaway robots are the most common source of injury on our team in previous
  years.

- While testing, no matter what you are doing, **never ever point a robot's turret in the direction
  of anything you don't want to shoot**.
    -  In particular, this includes monitors or other breakable devices (and humans of course). The
       pellets can easily damage computer screens.

- Always **ensure you have an easy safe way to turn the robot off** in case of complete failure. If
  this means for testing having a battery outside the robot that can be turned off quickly and
  safely, do it.
    - When you do turn on a robot, have your hand over the battery switch so you can quickly turn it
      off.

- When possible, **only plug in motors and actuators that you are currently testing.** When you are
  sure your new code works, then you can move on to testing the whole system.

- Unless you have good reason to, **never step-through debug while the MCB is controlling motors.**
  Suppose you pause the program midway through execution somewhere. The motors have stopped
  receiving messages from the MCB, so they continue to execute the last command that has been given
  to them. If this means apply max current, this usually means the robot will go out of control.

### Safety practices while testing DJI motors

- When first testing motors, **comment out the line of code that sends messages to the motors
  (`Drivers::djiMotorTxHandler.processCanSendData();`)** and live watch the desired motor output of
  the motors you are testing. You can emulate the motor output by moving the motors by hand to
  ensure the desired output looks sane.

- After testing without sending messages to motors, you should next **limit the motor output to
  something less than the motor's maximum output.** For the GM6020 motor, whose maximum output is
  30000, I limit the the output to 15000, and for the GM3508, whose maximum output is 16000, I limit
  the output to 8000.

### Safety precautions while handling robots

- While a robot is powered on, **never look down the barrel of a turret.**

- While working with pneumatic systems, whether or not the robot is on, assume that the system is
  always operational.
    - For example, if you have a pneumatic cannon, assume that the cannon can fire at any time.

- Lithium ion batteries are notorious for exploding. Thankfully our club has never experienced this,
  but **if you ever see a lithium battery swelling or smoking, follow the safety procedure <a
  href="">here</a>.**

# One final note

Everything written above has been done so not just for fun. People have been injured by rouge robots
and unsafe testing conditions that could have been avoided if the above conditions were followed.
