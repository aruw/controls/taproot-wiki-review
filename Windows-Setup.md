Download/install the following tools:

- **Python 3.8 or greater**

  Notes:
  - **Do not** use Python 3.10. Many packages lack pre-built binaries for it.
  - **Do not** use the Python installation that comes with Windows. Install Python fresh.

  Check **Add Python 3.X to PATH** in the installer.

  Official download: https://www.python.org/downloads/

- **git**

  Official download: https://git-scm.com/downloads

- **Visual Studio Code**

  Official download: https://code.visualstudio.com/download

- **ARM GNU toolchain with gdb.**

  When the installation finishes, make sure to check "Add path to environment variable".

  Official download: https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads

  Select the win32 `.exe` ending in `-win32.exe`, e.g. `gcc-arm-none-eabi-10-2020-q4-major-win32.exe`.

- **OpenOCD**

  Official download: https://github.com/xpack-dev-tools/openocd-xpack/releases/

  Select the x64 Windows version ending in `-win32-x64.zip`, e.g. `xpack-openocd-0.10.0-15-win32-x64.zip`.

  Unzip it to a known location and [follow the instructions below](#appendix-adding-directories-to-your-path)
  to add the `bin` directory to your PATH. The full path should look like `C:\tools\xpack-openocd-0.10.0-15\bin`. 

- **ST-Link Driver**

  Official download: http://www.st.com/en/embedded-software/stsw-link009.html

  Google Drive mirror (currently private to ARUW): https://drive.google.com/file/d/1m4dv3zL_lruf89O9ft9EJCFlwj8BoZne/view?usp=sharing

  The "Google Drive mirror" should be preferred when possible due to ST's sign-up requirement.

- **gcc and g++ compilers**

  Official download: https://winlibs.com/

  Download the latest <u>GCC 11.X.X "UCRT" Win64</u> version as a ZIP file.

  Unzip it to a known location and [follow the instructions below](#appendix-adding-directories-to-your-path)
  to add the `mingw64/bin` directory to your PATH. The full path should look like
  `C:\tools\winlibs-x86_64-posix-seh-gcc-11.2.0-llvm-13.0.0-mingw-w64ucrt-9.0.0-r2\mingw64\bin`. 

  - **ClangFormat version 10**

     By default, there is a ClangFormat executable that comes with the gcc/g++ compiler listed above, but this
     executable is a newer version than what we want (currently it is version 13.0.0). Since there is not direct
     support for this version of ClangFormat on Linux, you must install an older version of ClangFormat as a Windows
     user.

     - Navigate to `C:\mingw64\bin` and rename `clang-format.exe` to `clang-format-13.exe`.

     - Download [`clang-format.exe`](https://drive.google.com/file/d/1iPyqSWEANHj8tusTIXXuoZav9u0P4Cs1/view?usp=sharing),
       a ClangFormat executable that is version 10.

     - Move `clang-format.exe` to `C:\mingw64\bin`.

- **googletest library**

  Download (custom build): https://github.com/uw-advanced-robotics/googletest-gcc-windows-builds/releases

  Download the latest release.

  Unzip it to a known location and then double-click `install.bat` in the root folder.

- (Optional)  **J-Link driver, Ozone, and J-Scope**

  The J-Link provides enhanced debugging capabilities, including graphing live data.
  This software is only needed if you plan on using a J-Link for development and debugging.

  Official download: https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack

- (Optional) **Windows Terminal**

  This is entirely unnecessary, but makes it much more pleasant to use Command Prompt and/or
  PowerShell.

  Official download: https://docs.microsoft.com/en-us/windows/terminal/install

When asked to run commands in the future, you should do so in a PowerShell terminal (optionally,
within Windows Terminal).

**Close (and reopen if necessary) any existing terminal sessions or Visual Studio Code windows.**

Visit the README of the repo you are working in to find next steps. You'll want to:
- Clone your repository
- Build the project and run the tests to confirm your environment is set up correctly

## Appendix: Adding directories to your PATH

Commands run in the terminal use an "environment variable" called `PATH` to find runnable programs.
Most installers will automatically add their programs to PATH, but some of our tools don't have
installers, so we have to do it by hand.

To add one or more directories to `PATH`:
  - Open _Start_ and search for "environment" to find "Edit the system environment variables". Click
    "Environment Variables...".
  - Double-click `Path` under the "User variables for &lt;Username&gt;"

    <details>
    <summary>Click to see screenshot</summary>
    <img src="uploads/windows_path_environment_var.jpg" height="500px">
    </details>
  - Click "New" and paste in the absolute path to the folder you want to add. Usually, it's a path
    to a folder called `bin`, e.g. `C:\tools\xpack-openocd-0.10.0-15\bin`. You can do this for
    multiple paths.

    <details>
    <summary>Click to see screenshot</summary>
    <img src="uploads/windows_env_var_new_button.jpg" height="500px">
    </details>

    <details>
    <summary>Click to see screenshot</summary>
    <img src="uploads/windows_env_var_new_path_entry.jpg" height="500px">
    </details>
  - Click "Ok" twice
