Looking for something else or would like to contribute to the wiki?

This wiki is a readonly mirror of our
[GitLab](https://gitlab.com/aruw/controls/taproot/-/wikis/home) wiki. We use [mermaid
diagrams](https://mermaid-js.github.io/mermaid/#/) in this wiki, which are not supported in GitHub.
We recommend referring to the GitLab wiki for the best experience or if you would like to
contribute.
