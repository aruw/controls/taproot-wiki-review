The below instructions have been tested on macOS Catalina 10.15.7 on an x86 processor as well as macOS Big Sur on Apple M1.

1. Install Homebrew:

   ```sh
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   brew update
   ```

1. Install various system-level dependencies.

   Note that you may need to upgrade your system using the `softwareupdate` command or install xcode
   if some of the below installations fail. 

   ```sh
   # install general tools
   brew install python3 git

   # install ARM (target device) toolchain and tools
   brew tap osx-cross/arm
   brew install arm-gcc-bin
   brew install openocd --HEAD

   # install host gcc compiler and boost lib for tests
   brew install boost gcc@11 cmake
   ```

1. Choose a directory to store your source code. `repos` is a good choice:

   ```sh
   mkdir repos
   cd repos
   ```

1. Build and install googletest:

   ```sh
   git clone --branch release-1.10.0 https://github.com/google/googletest.git
   cd googletest
   mkdir build
   cd build
   cmake -DCMAKE_CXX_COMPILER=g++-11 -DCMAKE_C_COMPILER=gcc-11 ../
   make
   sudo make install
   cd ../../ # Do not forget this step
   ```

1. Install `clang-format-10`:

   Download it from here: https://github.com/llvm/llvm-project/releases/download/llvmorg-10.0.0/clang+llvm-10.0.0-x86_64-apple-darwin.tar.xz

   Place the downloaded `clang+llvm-10.0.0-x86_64-apple-darwin` in a good location and add the path `bin` directory to your `.zshrc` file:

   ```sh
   export PATH="/path/to/Users/stlp/uw/aruw/repos/clang+llvm-10.0.0-x86_64-apple-darwin/bin:$PATH"
   ```

Visit the README of the repo you are working in to find next steps. You'll want to:
- Clone your repository
- Build the project and run the tests to confirm your environment is set up correctly
