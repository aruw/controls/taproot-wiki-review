## Table of Contents
- [Table of Contents](#table-of-contents)
- [Introduction](#introduction)
- [Installation Steps](#installation-steps)
- [Driving a Robot Using the Client](#driving-a-robot-using-the-client)
  - [Prerequisites](#prerequisites)
  - [Configuring the VTM12 (Client-side VTM)](#configuring-the-vtm12-client-side-vtm)
  - [Launching the Client](#launching-the-client)
    - [Troubleshooting](#troubleshooting)

## Introduction

A guide on how to setup the RoboMaster Client. This is a piece of software distributed by RoboMaster
which pilots use during the match to, well... pilot the robots.

We highly recommend having the RoboMaster Referee System User Manual handy while going through this
guide. It is the original source of a lot of the information for Referee Client setup. This guide
fills in some knowledge gaps and helps with more specific details of the setup.

## Installation Steps
1. Download the RoboMaster Client archive from here: 
   https://drive.google.com/file/d/1rCdiCaSDUfABEGa9kWwP27sZImwIm2uW/view
2. Extract the RAR into a folder of your choosing
3. You're done! The `RoboMasterClient.exe` is the client

The hard part is getting it linked with a server and robot properly.

## Driving a Robot Using the Client

### Prerequisites
- A Robot to drive which: 
  - Has an activated RoboMaster Video Transmitter Module 02 (VTM02). Used for client video display.
    Instructions for how to activate it are in referee system user manual.
  - Can take keyboard input. See table 2-1 in the Robomaster Robot Building Specification manual for
    an overview of the potential sources of input for a competition-legal robot.
- An activated VTM receiver (VTM12) for the Client-side. Details on how to set this up are in the
  ref system user manual. Confusingly RoboMaster named two separate things the "VTM". The one that
  connects to your computer is the VTM12. This is what communicates with the other VTM and allows
  you to see video on the client side. It also sends keyboard input should you choose to get input
  from the VTM.
- A RoboMaster server setup. See the RoboMaster Server Setup guide in this wiki for details.

### Configuring the VTM12 (Client-side VTM)

The IP Address of the VTM12 must be `192.168.42.105`. The image in the Referee System User Manual
also indicates that the subnet mask should be set to `255.255.255.0`

Here is how to do it on Windows:
1. Make sure VTM12 is plugged in to your computer
2. Control panel -> Network and Sharing Center:
3. Locate the unidentified network (from the VTM), open its settings
   ![Locating the VTM network interface](/uploads/client-setup/VTM-network-and-sharing-center.PNG)

4. Select "Internet Protocol Version 4" and click "properties" (or just double click "Internet
   Protocol Version 4").

   ![Locating the properties of the VTM network
   interface](/uploads/client-setup/VTM-ethernet-properties.PNG)

5. From here follow the Referee System user manual and set the IP to 192.168.42.105, (subnet is
   255.255.255.0)

### Launching the Client

1. Make sure you are connected to the same Local Area Network as the server.
2. Launch `RoboMasterClient.exe` with the following settings
   - Screen resolution: 1920 x 1080 (Things just break otherwise. VTM has issues, custom graphics
     break...)
   - Graphics quality: HighFPS
   - Monitor: whichever monitor you want to display too. Default should work.
   - Not windowed
3. Follow referee system user manual from here on how to connect client to robot.

Once you've finished following the Referee System User Manual and connected successfully you should
be able to send keyboard input to your robot and see the VTM video feed through the client!

#### Troubleshooting

- Grey screen even though client HUD indicates VTM is connected? Give it a minute. Still not
  connected? Many issues are solved by turning VTM receiver and transmitter on and off again.
- Make sure you have matching robot color and ID selected on the client and on the Referee System
  Main Control Module.
