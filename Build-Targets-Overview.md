When building your code with `taproot` and the provided build tooling, there are a few `target`s
that one can select to build. In particular, you may choose to build for either the hosted or MCB
target. While running on the hosted target, you may choose to run _either_ the unit test suite or
the simulator. For the discussion below, note that the directory structure of your project is assumed to be as follows:

```
|-- <your project name>-project
|   |-- taproot
|   |   |-- src                 (directory containing taproot source code)
|   |   |-- test                (directory containing taproot tests)
|   |-- src                     (directory containing your source code)
|   |-- test                    (directory containing your tests)
|   |-- SConstruct              (build file)
```

All paths referenced below will assume your current working directory is in `<your project name>-project`.

## Hosted Target

The hosted target is defined [here](Definitions#hosted-target). Please read this definition before
reading the next section. In the codebase, when the hosted target is enabled, the macro
`PLATFORM_HOSTED` is defined.

### "Simulated MCB" Environment

The simulated MCB environment runs the main MCB code on the hosted target (i.e. `main` is run). The
simulator also encompasses any periphery programs used to fake
[GPIO](Definitions#general-purpose-input/output-pin-(gpio)) devices that communicate with the MCB.
This is (currently) independent of any simulation CV does.

### Unit Test Environment

The unit test environment runs all the unit tests in `/test` and `/taproot/test`.
Everything in `/src` and `/taproot/src` is compiled except for `main.cpp` In the codebase, when
the unit test environment is enabled, the `ENV_UNIT_TESTS` macro is enabled.

## MCB Target

Targeting the MCB during compilation simply means that you intend to deploy the code to the MCB.
There are no explicit macros in the codebase used to indicate we are building on the MCB target.
Instead, use the `PLATFORM_HOSTED` target to differentiate between the MCB and hosted target.
