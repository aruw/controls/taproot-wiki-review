# Fedora Linux Setup

1. Install system dependencies as follows:
    ```sh
    sudo dnf install python pip scons git doxygen make automake gcc gcc-c++ stlink \ 
                openocd kernel-devel boost-devel python3-virtualenv \
                arm-none-eabi-gcc-cs arm-none-eabi-gcc-cs-c++
    ```


1. Choose a directory to store your source code. `repos` is a good choice:

   ```sh
   mkdir repos
   cd repos
   ```

1. Build and install googletest:

   ```sh
   git clone --branch release-1.10.0 https://github.com/google/googletest.git
   cd googletest
   mkdir build
   cd build
   cmake ../
   make
   sudo make install
   cd ../../
   ```

1. Download and install [VSCode](https://code.visualstudio.com/download).

Visit the README of the repo you are working in to find next steps. You'll want to:
- Clone your repository
- Build the project and run the tests to confirm your environment is set up correctly
