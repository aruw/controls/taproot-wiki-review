**[Home](Home)**

**Architecture Design**

- [Directory Structure](Directory-Structure)
- [Build Targets Overview](Build-Targets-Overview)
- [Drivers Architecture](Drivers-Architecture)
- [Command Subsystem Framework](Command-Subsystem-Framework)
- [Generated Documentation](https://aruw.gitlab.io/controls/taproot/)

**Using Taproot**
- [Upgrading a Taproot project](Upgrading-a-Taproot-project)
- [Code Generation in User Projects](Code-Generation-in-User-Projects)

**Software Tools**

- [Docker Overview](Docker-Overview)
- [Debugging Safety Information](Debugging-Safety-Information)
- [Debugging With ST-Link](Debugging-With-STLink)
- [Debugging With J-Link](Debugging-With-JLink)
- [Git Tutorial](Git-Tutorial)
- [How to Chip Erase the MCB](How-to-Chip-Erase-the-MCB)

**RoboMaster Tools**

- [RoboMaster Server Setup](RoboMaster-Server-Setup)
- [RoboMaster Client Setup](RoboMaster-Client-Setup)
- [DT7 Remote Firmware](DT7-Firmware-Upgrading)

**Software Profiling**

- [Profiling Main](Profiling-Main)
- [Profiling Matrix Operations](Profiling-Matrix-Operations)

**System Setup Guides**

- [Windows Setup](Windows-Setup)
- [Debian Linux Setup](Debian-Linux-Setup)
- [Fedora Linux Setup](Fedora-Linux-Setup)
- [macOS Setup](macOS-Setup)
- [Docker Container Setup](Docker-Container-Setup)
- (deprecated) [Windows WSL Setup](Windows-WSL-Setup)

**Control System Design Notes**

- [Simple Turret Control](Simple-Turret-Control)

**Miscellaneous and Brainstorming**

- [Style Guide](C++-ARUW-Style-Guide)
- [Unit Test Guide](Unit-Test-Guide)
- [Definitions](Definitions)

_Submit edits to this wiki via the [taproot-wiki-review repo](https://gitlab.com/aruw/controls/taproot-wiki-review)._
