## Introduction

This document serves as a guide on how to set up the RoboMaster server. The server may be used to
simulate robot matches in an environment similar to the real thing. Also the server is necessary for
testing the Referee Serial GUI protocol. These directions exist because there is currently no good
step by step procedure written in English that is accessible. These instructions are only applicable
if you are running the RoboMaster server on a Windows 10 or 11 machine.

## Wifi Setup

1. For this setup you will need access to a standard off the shelf router with an admin panel. You will also want a router that has enough bandwith capability to talk to 7+ devices on the network simultaneously to connect to robots and your driver computers. This router must be a 2.4Ghz router, as the ref system is incompatible with 5Ghz. ARUW uses a residential Netgear router for this. 

2. Set the SSID to anything you would like that you can find on the robots. This should be a name without spaces to prevent issues, but it is not specified by DJI. 

3. Set the password to `12345678`. This is a hardcoded requirement in the robots. If the password is set to anything else, the robots will not connect.

## Setting up the Server

1. Download the latest version of the RoboMaster Server (the 2022 server can be found
   [here ](https://www.robomaster.com/zh-CN/products/components/detail/3212) for RMUC and [here](https://www.robomaster.com/zh-CN/products/components/detail/3208) for RMUL, but make sure you have the latest version).

2. Turn off the WI-FI on your computer.

3. Connect the router to the computer that will run the RoboMaster server (your computer) with an
   ethernet cable. Ensure the ethernet cable is plugged into one of the four leftmost black ethernet
   ports on the router. With the ethernet plugged in you should see the following in your Ethernet
   settings page (to navigate to this page, hit the Windows button and type "Ethernet settings" and
   hit enter). Note that seeing "Unidentified network: No Internet" is expected.

   <img src='./uploads/ethernet-page.PNG'>

4. Set the IP address of your computer to "192.168.1.2". To do this, while in the "Ethernet
   settings" page (see previous instruction), click on "Change adapter options" on the right hand
   side of the page. While in this page, select "Ethernet", then select "Properties".

   <img src='./uploads/ethernet-adapter-options.PNG'>

   <img src='./uploads/ethernet-status-page.PNG'>

   In the properties page scroll down and double click on "Internet Protocol Version 4 (TCP/IPv4)"

   <img src='./uploads/ethernet-properties.PNG'>

   In the IPV4 Properties, select "Use the following IP address", and then set the IP address to
   "192.168.1.2" and the Subnet mask to "255.255.255.0" (as shown below).

   <img src='./uploads/ipv4-properties.PNG'>

5. Turn off your firewall. Click the Windows button then type in "Windows Defender Firewall" and hit
   enter. From this page, select "Turn Windows Defender Firewall on or off."

   <img src='./uploads/firewall-off.png'>

6. Start the RoboMaster server (`RoboMasterServer.exe`). When it has started select "StartAll".

7.  You may now start the RoboMaster Client, which can be downloaded from the same location as the servers and log into the server on any computer connected through ethernet or Wi-Fi to the router (including the computer running the server). 

8.  Refer to the referee system documentation for how to connect a robot to the server. With server
    configured correctly, the client should automatically accept robots when they are connected to
    the router.

## Troubleshooting

If you run into the following issue when attempting to connect your a robot to the server, try
turning off your firewall and restarting your computer with firewalls turned off.

<img src='./uploads/server-firewall-bug.PNG'>
