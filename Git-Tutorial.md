## Overview

The following will go over some simple git commands necessary for cloning a git repository, creating
a new branch, adding and committing new changes to a branch, and pushing the branch to a remote. For
this tutorial, I will use [Git Bash](https://git-scm.com/downloads) and some
[VSCode](https://code.visualstudio.com/download). If you have done so already and want to use these
tools, download and install both.

## Prerequisites

- Read [debugging safety information](./Debugging-Safety-Information).
- Read the [ST-Link tutorial](./Debugging-With-STLink).


## Cloning a repository

When you start working on one of our repositories, the first step is to clone it. Open up a new Git
Bash terminal (in the start menu, search for "Git Bash" and select the application that comes up).
See the image of the application below.

<img src="uploads/git_bash_application.png" alt="git bash" class="center" height="200">

In Git Bash, you must then navigate to where you would like the repository to be downloaded to. Out
of personal preference, I like to keep all my ARUW-related repositories together. To navigate, type
`ls` to see the folders and files in the current folder you are in (folders end in `/`), and type
`cd folder-name` to step into the folder `folder-name`.

In the preferred folder, type `git clone url `where `url` is the https of the repository (see the
image below if you are unsure of where to find this). You can instead clone with SSH, which requires
setting up an SSH key on your device. If https clone fails, see [this ssh
tutorial](https://docs.gitlab.com/ee/ssh/) to set up an SSH key for cloning. You will then click on
the link under "Clone with SSH" once the key is set up.

<img src="uploads/aruw-edu-clone.PNG" alt="aruw edu clone">

Next `cd` into the repository.

## Checking out a new branch

Now suppose you want to start writing some code. You don't want to start writing code to the main
branch (usually `develop` or `master`), so instead you can create branch off of the main branch, add
and modify code on the branch you created, then push your own branch to the central server. This can
later be merged back into the main branch once you are complete with your modifications.

The following explains how to create a new branch.

1. Open up Git Bash.
2. Navigate to the repository (using `cd`).
3. For our repositories we make branches off of the `develop` branch. So type `git checkout develop`
   to move to that branch.
3. Type `git checkout -b branch-name`. In our main repo, we typically use the naming convention
   `FirstnameL/Issue#/description`, though check with other members to insure the naming convention
   you are using is correct.

## Checking out an existing branch

In case you want to switch branches from where you already are to one that already exists, follow
the steps below.

1. Open up Git Bash and navigate to the repository.
2. Make sure you have [committed](#adding,-committing,-and-pushing-your-code) or
   [stashed](https://git-scm.com/book/en/v2/Git-Tools-Stashing-and-Cleaning#:~:text=The%20answer%20to%20this%20issue,even%20on%20a%20different%20branch)
   your code. 
3. Type `git branch -a` to see all local and remote branches (only necessary if you are unsure of
   the name of the branch you want to check out)
4. Type `git checkout branch-name`. If `branch-name` is a valid branch name in the repository you
   are working in, you should now have the requested branch checked out.
5. Type `git pull` if you want the latest changes on the branch you have newly checked out.

## Adding, committing, and pushing your code

Suppose you have written some code and want to add your code to the central server. Here are a few
reasons why you should do this:
- As a back up in case your computer crashes.
- As a way to "go back in time" in case you mess up some code and want to undo recent changes.
- So others can see and work on the same code you are working on. I highly recommend committing your
  code frequently (at least once each day you work on it, I personally do it much more than that).

I will explain one way of pushing your code to the branch you currently have checked out.

1. Open up Git Bash and navigate to the repository.
2. You can now type `git status` if you would like to see the files that are changed. These files
   are not yet staged, so you must stage them. You can either stage changes individually by typing `
   git add filename` or just stage all changes by typing ` git add .`. Alternatively, with the
   [VSCode project folder](Definitions#vscode-project-folder) open, navigating to the "source
   control" tab will allow you to see how the changes you have made compare to the changes on the
   upstream branch (i.e. the server). There are a lot of helpful VSCode features, but in particular,
   to add files, similar to `git add filename`, you can click the "+" next to the file.
   Additionally, selecting a file from the source control panel will show you the changes you have
   made locally. The local changes will be on the right and upstream file on the left. Additions
   will be in green and deletions in red. <br><br>
   <img src="uploads/source-control-diff-image.PNG" alt="vscode-source-control"> <br>

5. Now you must commit the staged changes. Type `git commit -m "Type a message describing what
   changes have been made."`
6. Finally, type `git push`. If the branch you are pushing is not upstream (not on the repo yet
   since you just made it locally), git will complain, and you will have to type what it says to
   type :). 

I briefly mentioned using VSCode to interact with git in one of the steps, but you can actually do a
lot more through VSCode as well. You can look up online how to use it or just explore VSCode and see
what features there are. I also recommend the [GitLens extension](https://gitlens.amod.io/) for
VSCode, which adds additional features built on top of Git.
