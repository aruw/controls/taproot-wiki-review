## Overview

There are a couple of ways to debug on the MCB. This tutorial we will cover what software is
necessary for effectively for debugging and some important information you should know while
debugging.

## Prerequisites

- Read [debugging safety information](./Debugging-Safety-Information).

## Necessary software

In addition to the general software requirements described one of the setup guides in this wiki (see
the "System Setup Guides" section on the sidebar), it is important to have the ST-Link V2 driver.
You may have already installed this driver, but if you have not go back to the system setup guide
for the operating system you have and follow the steps to install the ST-Link V2 driver.

## How to debug

Let us start by plugging in the ST-Link to the MCB. An image with the JTAG port boxed on the MCB is
shown below. When you plug in, make sure any external power supply is turned off.

<img src="uploads/robomaster_type_a_board.PNG" alt="robomaster board" width=400>

Once everything is plugged in successfully, if this is the first time using an ST-Link, you should
try to upload some code to insure this works before trying to debug. To do so, with VSCode open in
the [project folder](Definitions#vscode-project-folder) for whichever repository you are trying to
build type <kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>p</kbd>, then select "Tasks: Run Task", then
"Program - Debug". If you are successful, the command prompt should look something like this:

```
scons: Reading SConscript files ...   
Configured 8 parallel build jobs (-j8)
scons: done reading SConscript files. 
scons: Building targets ...
.----OpenOCD---build\debug\aruw-edu.elf
'------------->stm32f427iih
scons: done building targets.

Open On-Chip Debugger 0.10.0 (2019-08-28) [https://github.com/sysprogs/openocd]
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD      
modm_program
Info : clock speed 2000 kHz
Info : STLINK V2J33S7 (API v2) VID:PID 0483:3748
Info : Target voltage: 3.240318
Info : stm32f4x.cpu: hardware has 6 breakpoints, 4 watchpoints
Info : Listening on port 3333 for gdb connections
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x08002850 msp: 0x10000be0
Info : Unable to match requested speed 8000 kHz, using 4000 kHz
Info : Unable to match requested speed 8000 kHz, using 4000 kHz
** Programming Started **
Info : device id = 0x20016419
Info : flash size = 2048 kbytes
Info : Dual Bank 2048 kiB STM32F42x/43x/469/479 found
Info : Padding image section 1 at 0x080001b8 with 8 bytes
** Programming Finished **
** Verify Started **
** Verified OK **
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
shutdown command invoked


Press any key to close the terminal.
```

Also, **the ST-Link's LEDs will flash blue and red while a successful data transfer is occurring
between the computer and ST-Link**.

### Troubleshooting

If you are unable to successfully program the MCB, there are a number of potential reasons. Here are
a few questions that you should ask yourself:

- Does your code build?
- Are you in the correct repository? By this I mean have you opened the [project
  folder](Definitions#vscode-project-folder) in VSCode and not any other folder. It is common to
  confuse the project folder with a similarly named sub-folder.
- Is the ST-Link connected properly? On windows, go to device manager to insure the ST-Link is
  recognized by your device.
- Did you properly install the ST-Link V2 driver? It is not enough to just download the driver. An
  installation step is also necessary.
- Sometimes the ST-Link is shorted. If you are certain that none of the above issues are the reason
  why you cannot connect to the MCB, this is a likely cause.

In particular, if an ST-Link is detected and an MCB is not detected, the following failure message
will be printed to the console.

```
ERROR conda.cli.main_run:execute(32): Subprocess for 'conda run ['scons', 'program', 'profile=debug']' command failed.  (See above for error)
scons: Reading SConscript files ...     
Configured 8 parallel build jobs (-j8)  
scons: done reading SConscript files.   
scons: Building targets ...
Compiling C++·· build\debug\src\main.o  
Linking········ build\debug\aruw-edu.elf
.----OpenOCD---build\debug\aruw-edu.elf
'------------->stm32f427iih
scons: building terminated because of errors.

Open On-Chip Debugger 0.10.0 (2019-08-28) [https://github.com/sysprogs/openocd]
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD      
modm_program
Info : clock speed 2000 kHz
Info : STLINK V2J33S7 (API v2) VID:PID 0483:3748
Info : Target voltage: 3.233952
Error: init mode failed (unable to connect to the target)
aruw-mcb\aruw-mcb-project\modm/openocd.cfg:13: Error:
in procedure 'modm_program'
in procedure 'program' called at file "aruw-mcb\aruw-mcb-project\modm/openocd.cfg", line 13
** OpenOCD init failed **
shutdown command invoked

scons: *** [openocd_run] Error 1


Press any key to close the terminal.
```

In particular, note `unable to connect to the target`. Above this line you can see that connection
to the ST-Link is successful but then the target connection failed.

If instead the connection to the ST-Link fails, you will see an error such as this printed to the
console:

```
ERROR conda.cli.main_run:execute(32): Subprocess for 'conda run ['scons', 'program', 'profile=debug']' command failed.  (See above for error)
scons: Reading SConscript files ...
Configured 8 parallel build jobs (-j8)
scons: done reading SConscript files.
scons: Building targets ...
.----OpenOCD---build\debug\aruw-edu.elf
'------------->stm32f427iih
scons: building terminated because of errors.

Open On-Chip Debugger 0.10.0 (2019-08-28) [https://github.com/sysprogs/openocd]
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD      
modm_program
Info : clock speed 2000 kHz
Error: open failed: jtag_libusb_open() returned error -4
C:\Users\Matthew\Documents\UW\ARUW\repos\aruw-edu\aruw-mcb\aruw-mcb-project\modm/openocd.cfg:13: Error:
in procedure 'modm_program'
in procedure 'program' called at file "C:\Users\Matthew\Documents\UW\ARUW\repos\aruw-edu\aruw-mcb\aruw-mcb-project\modm/openocd.cfg", line 13
** OpenOCD init failed **
shutdown command invoked

scons: *** [openocd_run] Error 1


Press any key to close the terminal.
```

In particular, note the line where the error occurred (the line `Error: open failed:
jtag_libusb_open() returned error -4`) You can see that the program responsible for interfacing with
the ST-Link failed to open a socket for communicating with the ST-Link.

I have now covered the most common errors and hopefully you have an idea of how to understand what
exactly is wrong if uploading fails. Inspecting the build output printed to the terminal can be very
useful. If you still have trouble uploading ask other members.

## Debugging tips

Suppose you have successfully uploaded code to the MCB. Let us move on to debugging. Debugging via
the ST-Link has been integrated into both aruw-edu and the main repository. To start a debug
session, open the [VSCode project folder](Definitions#vscode-project-folder). Next navigate to the
debug panel (<kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>D</kbd>). Click on the green run button to
start the debug session. If successful, the session will be in a paused state and you will see the
debug tool shown below. If you need to turn on an external power supply, do so now.

For more information about the debugger we are using (Cortex-Debug), look into the [overview
here](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug). This is a pretty
put together debugger, though there are a few things that you should know about debugging on an
embedded system.

- As mentioned above, while using the ST-Link it is important to turn off external power to the MCB
  before plugging in the ST-Link. Not only can it affect the success of uploading code, but also we
  have shorted quite a few ST-Links by plugging them into the MCB with external power still on.

- There is no exception handling, so if there is a fatal error, while on linux you may get a seg
  fault, on the MCB the best possible scenario is that the code will enter a failure handler
  (located in `rm-dev-board/board.cpp`) and then the program will restart. Again, I say hopefully
  because the other option is the robot starts going crazy as random parts of memory are corrupted
  (there is usually limited read write protection on an embedded system). If your code keeps
  restarting, you can set a breakpoint in this failure handler and then investigate the
  <code>char*</code> arrays that are located in the handler, as they sometimes give insight into
  what happened.
- While using the ST-Link, the number of breakpoints you can set is limited by the hardware. I
  believe you cannot set more than 6 breakpoints before debugging starts to act in undefined ways.
- While step through debugging, unexpected behavior can occur depending on what compilation
  optimizations have been made. In particular, if the debugger appears to skip lines while step
  through debugging, a likely explanation is that those lines are still being run but have been
  optimized away.
- Another optimization related note: usually the compiler optimizes out variables that are not used
  or are set but never referenced. If you attempt to watch a variable declared within a function,
  but the debugger determines it is "out of scope," declare the <a
  href="https://www.informit.com/articles/article.aspx?p=1708657&seqNum=3#:~:text=You%20can%20declare%20global%E2%80%94that,the%20end%20of%20the%20file.">variable
  globally</a>.
