This setup is not recommended because we still do not support deploying to the development board
from the Docker Container, However, this guide is still complete and will allow you to build code
for all supported targets and run unit tests.

1. [Install Docker Desktop](https://docs.docker.com/get-docker/). The macOS and Linux instructions
   will work as-is. If you are on Windows, there are two options:
     - If you are on Windows 10 version 2004 or later ([how do I
       know?](https://support.microsoft.com/en-us/help/13443/windows-which-version-am-i-running)),
       the relevant guide is
       [here](https://docs.docker.com/docker-for-windows/install-windows-home/). Make sure you click
       "Enable WSL 2 Features" in the Docker installer.
     - If you cannot update to Windows 10 version 2004 (recommended) but are running Windows 10 Pro
       (not Home), you can follow [this guide](https://docs.docker.com/docker-for-windows/install/)
       to use the Hyper-V backend instead.
2. [Install Visual Studio Code](https://code.visualstudio.com/).
3. [Install git](https://git-scm.com/).
4. Open Visual Studio Code.
5. On the left of the editor, click the "extensions" icon (looks like stacked squares). Search for
   "Remote - Containers" and press "Install".

   <img
   src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/082462149c27caeba5aad30f5a504285/image.png"
   width="500px">
6. Copy this URL onto your clipboard:
   `https://gitlab.com/aruw/controls/taproot-template-project.git`
7. Press <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd> and search for "Remote-Containers: Clone
   repository in container volume...". Press enter.

   <img
   src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/b68f8856c603e9790277a465099840eb/image.png"
   width="500px">
8. Paste the URL you copied previously. DO NOT select the "GitHub" shortcut. Press enter.

   <img
   src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/015fef7572f62c15b0497b59dba53201/image.png"
   width="500px">
9. Select "Create a unique volume".

   <img
   src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/1c662da207f1345ddaf0738a64caee4d/unique_volume.png"
   width="500px">
10. Wait for vscode to initialize. This may take minutes, since it is downloading all the required
    tools in the form of a multi-gigabyte compressed file system image.<br>
    - **Note:** During this time, if you are having trouble with your git credentials, you can
      specify or update them through a Windows GUI. If you're on windows open the **Credential
      Manager** app and go to **Windows Credentials**. Then correct your git credentials stored
      under `git:https://gitlab.com` before trying to clone again.

Now that you have the environment, let's test it out! Press
<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd>, type "Focus Next Terminal", and press
<kbd>Enter</kbd>. In this terminal, type `cd template-project && scons run-tests -j4` and press
<kbd>Enter</kbd>. After building our code, it should run the tests and print a message indicating
that all tests passed.

## Reconnecting to the Docker container in the future

Following the above instructions again _will_ work, but is not the most efficient way to reopen the
container. Instead, from a new vscode window, select it from the "recent" menu:

<img src="uploads/reopen_dev_container_from_recent.png" width="250px">

Alternately, you can do so via the "Remote Explorer" pane:

<img src="uploads/reopen_dev_container_from_pane.png" width="250px">

## Optional: prevent Docker Desktop from running on startup

By default, Docker will likely be configured to run on startup. Your mileage may vary, but this may
be frustrating. If so, you can disable it! Open the Docker Desktop settings, which on Windows can be
accessed via the Docker icon in the system tray:

<img
src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/851f134487d0743bb857c67c4adbbc43/image.png"
width="250px">


The "General" tab has a checkbox for disabling auto-start.

<img
src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/3f62cd700d1f47632df92d3c96accffd/image.png"
width="500px">


In the future, if you attempt to load the repository within the dev container and haven't manually
started Docker, you will get the following error:

<img
src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/7e0672866e62f550c9f91b44d021e40b/image.png"
width="500px">


In this case, you can launch Docker manually and hit "Retry". On Windows, Docker Desktop can be
started by searching for "docker" in the Start menu.

<img
src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/00f31defc4d7855b40777816a4962f12/image.png"
width="300px">

**We also recommend you _stop_ Docker when you're done! This can be done on Windows via the same
icon in the system tray. This will help preserve battery life and RAM.**
