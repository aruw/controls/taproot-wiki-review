Sometimes it is necessary to perform a chip erase on the MCB. The following guide will walk through
how this is done.

1. The tool that we use to chip erase is ST-Link Utility. Download and install this tool
   [here](https://www.st.com/en/development-tools/stsw-link004.html). You also must have the ST-Link
   driver installed. See the [main repo's
   readme](https://gitlab.com/aruw/controls/aruw-mcb/-/blob/develop/README.md) for download and
   install instructions.
2. Open up ST-Link Utility. With the MCB connected via and ST-Link, select "Connect to the target"
   (see the image below). <br><br>
   <img src="uploads/st_link_utility_1.png" alt="st link utility connect to the target" width=700>
   <br> 
3. Once connected, select "Full chip erase," as seen below. Select "OK" to the popup that comes up.
   It should take a few seconds for the erase to be completed. <br><br>
   <img src="uploads/st_link_utility_2.PNG" alt="st link utility chip erase" width=700>
